package cl.psepulvedao.core.ui.listener

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

abstract class PaginationScrollListener(
    private val layoutManager: LinearLayoutManager,
    private val positionsBeforeLoadMore: Int
): RecyclerView.OnScrollListener() {

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        val totalItemCount = layoutManager.itemCount
        val fistVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()
        val loadMore = totalItemCount <= (fistVisibleItemPosition + positionsBeforeLoadMore)

        if(!isLoading() && !isLastPage() && loadMore) {
            loadMoreItems()
        }
    }

    abstract fun loadMoreItems()
    abstract fun isLastPage(): Boolean
    abstract fun isLoading(): Boolean
}