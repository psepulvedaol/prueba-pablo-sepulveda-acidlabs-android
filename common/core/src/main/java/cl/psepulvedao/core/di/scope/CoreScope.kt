package cl.psepulvedao.core.di.scope

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.SOURCE)
annotation class CoreScope