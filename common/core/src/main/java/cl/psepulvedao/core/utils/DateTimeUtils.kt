package cl.psepulvedao.core.utils

import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

object DateTimeUtils {
    const val USA_FORMAT = "yyyy-MM-dd"
    const val FULL_DATE_FORMAT = "dd MMM yyyy"

    val esESLocale = Locale("es", "ES")
    val parser = SimpleDateFormat.getDateInstance() as SimpleDateFormat
    val formatter = SimpleDateFormat.getInstance() as SimpleDateFormat

    fun formatDate(
        date: String,
        formatIn: String = USA_FORMAT,
        formatOut: String = USA_FORMAT
    ) : String {
        parser.applyPattern(formatIn)
        formatter.applyPattern(formatOut)

        return try {
            val parsedDate = parser.parse(date) ?: Date()
            formatter.format(parsedDate)
        } catch (ex: Exception) { "" }
    }
}