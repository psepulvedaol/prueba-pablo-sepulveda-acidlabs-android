package cl.psepulvedao.core.di

import cl.psepulvedao.core.di.modules.NetworkModule
import cl.psepulvedao.core.di.scope.CoreScope
import dagger.Component
import retrofit2.Retrofit

@CoreScope
@Component(modules = [NetworkModule::class])
interface CoreComponent {
    fun retrofit(): Retrofit
}