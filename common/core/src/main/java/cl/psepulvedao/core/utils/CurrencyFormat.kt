package cl.psepulvedao.core.utils

object CurrencyFormat {

    fun formatUSD(amount: Long): String {
        val formatted = String.format("%,d", amount)
        return "$$formatted"
    }

}