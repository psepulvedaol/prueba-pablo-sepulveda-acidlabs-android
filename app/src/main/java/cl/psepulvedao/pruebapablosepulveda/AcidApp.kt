package cl.psepulvedao.pruebapablosepulveda

import android.app.Application
import cl.psepulvedao.core.di.CoreComponent
import cl.psepulvedao.core.di.DaggerCoreComponent
import cl.psepulvedao.movie.presentation.di.component.DaggerMovieListComponent
import cl.psepulvedao.movie.presentation.di.component.MovieListComponent
import cl.psepulvedao.movie.presentation.di.component.MovieListComponentProvider
import cl.psepulvedao.pruebapablosepulveda.di.DaggerAppComponent

class AcidApp: Application(), MovieListComponentProvider {

    private lateinit var coreComponent: CoreComponent
    private lateinit var movieListComponent: MovieListComponent

    override fun onCreate() {
        super.onCreate()
        initDagger()
    }

    private fun initDagger() {
        initAppDependencyInjection()
        initCoreDependencyInjection()
        initMovieDependencyInjection()
    }

    private fun initAppDependencyInjection() {
        DaggerAppComponent
            .builder()
            .build()
            .inject(this)
    }

    private fun initCoreDependencyInjection() {
        coreComponent = DaggerCoreComponent.create()
    }

    private fun initMovieDependencyInjection() {
        movieListComponent = DaggerMovieListComponent
            .factory()
            .create(coreComponent)
    }

    override fun provideMovieListComponent(): MovieListComponent = movieListComponent
}