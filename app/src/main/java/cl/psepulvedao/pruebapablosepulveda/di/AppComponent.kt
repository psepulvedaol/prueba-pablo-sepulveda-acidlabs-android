package cl.psepulvedao.pruebapablosepulveda.di

import android.app.Application
import dagger.Component

@Component
interface AppComponent {

    fun inject(application: Application)

}