package cl.psepulvedao.movie.domain.di.module

import cl.psepulvedao.movie.di.scope.MovieScope
import cl.psepulvedao.movie.domain.repository.MovieRepository
import cl.psepulvedao.movie.domain.usecase.GetMovieListUseCase
import dagger.Module
import dagger.Provides

@Module
class MovieUseCaseModule {

    @MovieScope
    @Provides
    fun provideGetPopularUseCase(repository: MovieRepository) = GetMovieListUseCase(repository)

}