package cl.psepulvedao.movie.data.remote.response

import cl.psepulvedao.movie.data.remote.scheme.MovieMapper
import cl.psepulvedao.movie.domain.model.Movie
import cl.psepulvedao.movie.domain.model.MovieList

object GetMovieListResponseMapper {
    fun mapToModelMovieList(movieList: GetMovieListResponse): MovieList {
        val results = mutableListOf<Movie>()

        for(movie in movieList.results) {
            results.add(MovieMapper.mapToModelMovie(movie))
        }

        return MovieList(
            page = movieList.page,
            totalPages = movieList.totalPages,
            totalResults = movieList.totalResults,
            results = results
        )
    }
}