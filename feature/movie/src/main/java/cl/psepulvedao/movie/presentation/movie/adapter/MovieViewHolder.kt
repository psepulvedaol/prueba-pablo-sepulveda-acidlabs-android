package cl.psepulvedao.movie.presentation.movie.adapter

import androidx.recyclerview.widget.RecyclerView
import cl.psepulvedao.movie.databinding.ItemMovieBinding
import cl.psepulvedao.movie.domain.model.Movie
import com.bumptech.glide.Glide

class MovieViewHolder(private val binding: ItemMovieBinding, private val listener:(movie: Movie) -> Unit): RecyclerView.ViewHolder(binding.root) {
    fun bind(movie: Movie) {
        binding.tvMovieTitle.text = movie.title
        binding.root.setOnClickListener { listener(movie) }

        val url = "https://image.tmdb.org/t/p/w500${movie.posterPath}" // TODO Quitar hardcode
        Glide.with(binding.root.context)
            .load(url)
            .into(binding.imageView)
    }
}