package cl.psepulvedao.movie.presentation.di.component

import cl.psepulvedao.core.di.CoreComponent
import cl.psepulvedao.movie.data.di.module.MovieRepositoryModule
import cl.psepulvedao.movie.di.scope.MovieScope
import cl.psepulvedao.movie.domain.di.module.MovieUseCaseModule
import cl.psepulvedao.movie.presentation.detail.MovieDetailFragment
import cl.psepulvedao.movie.presentation.di.module.MovieViewModelModule
import cl.psepulvedao.movie.presentation.di.module.ViewModelFactoryModule
import cl.psepulvedao.movie.presentation.home.MovieHomeFragment
import cl.psepulvedao.movie.presentation.movie.MovieListFragment
import dagger.Component

@MovieScope
@Component(
    modules = [
        MovieRepositoryModule::class,
        MovieUseCaseModule::class,
        MovieViewModelModule::class,
        ViewModelFactoryModule::class, // TODO MOVER!
    ],
    dependencies = [CoreComponent::class]
)
interface MovieListComponent {

    @Component.Factory
    interface Factory {
        fun create(coreComponent: CoreComponent): MovieListComponent
    }

    fun inject(fragment: MovieListFragment)
    fun inject(fragment: MovieHomeFragment)
    fun inject(fragment: MovieDetailFragment)

}