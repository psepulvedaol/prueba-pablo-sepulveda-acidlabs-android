package cl.psepulvedao.movie.presentation.movie

import cl.psepulvedao.movie.domain.model.MovieList
import cl.psepulvedao.core.ui.base.BaseViewState

sealed class MovieListViewState : BaseViewState {

    object Loading: MovieListViewState()
    object Error: MovieListViewState()
    class LoadedPage(val page: Int, val movieList: MovieList): MovieListViewState()
}