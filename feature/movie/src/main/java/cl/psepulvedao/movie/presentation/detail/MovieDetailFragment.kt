package cl.psepulvedao.movie.presentation.detail

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.navigation.fragment.navArgs
import cl.psepulvedao.core.utils.CurrencyFormat
import cl.psepulvedao.core.utils.DateTimeUtils
import cl.psepulvedao.core.utils.DateTimeUtils.FULL_DATE_FORMAT
import cl.psepulvedao.movie.R
import cl.psepulvedao.movie.databinding.MovieDetailFragmentBinding
import cl.psepulvedao.movie.domain.model.Genre
import cl.psepulvedao.movie.domain.model.Movie
import cl.psepulvedao.movie.presentation.di.component.MovieListComponentProvider
import cl.psepulvedao.movie.presentation.movie.MovieListViewModel
import cl.psepulvedao.ui.extension.gone
import cl.psepulvedao.ui.extension.visible
import com.bumptech.glide.Glide
import javax.inject.Inject

class MovieDetailFragment : Fragment(R.layout.movie_detail_fragment) {

    @Inject
    lateinit var providerFactory: ViewModelProvider.Factory

    private lateinit var binding: MovieDetailFragmentBinding
    private val params: MovieDetailFragmentArgs by navArgs()
    private lateinit var viewModel: MovieDetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initDagger()
        viewModel = ViewModelProvider(requireActivity(), providerFactory).get(
            MovieDetailViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = MovieDetailFragmentBinding.bind(view)

        setupObservers()
        viewModel.getMovieDetail(params.movieId)
    }

    private fun setupObservers() {
        viewModel.state.observe(viewLifecycleOwner, ::handleViewState)
    }

    private fun handleViewState(viewState: MovieDetailViewState) {
        when(viewState) {
            MovieDetailViewState.Loading -> onLoadingDetail()
            is MovieDetailViewState.LoadedDetail -> onLoadedDetail(viewState.movie)
        }
    }

    private fun onLoadingDetail() {
        binding.progressBar.visible()
        binding.detailDataGroup.gone()
    }

    private fun showDetailData() {
        binding.progressBar.gone()
        binding.detailDataGroup.visible()
    }

    private fun onLoadedDetail(movie: Movie) {
        showDetailData()

        binding.tvMovieTitle.text = movie.title
        binding.tvReleaseDate.text = DateTimeUtils.formatDate(movie.releaseDate ?: "", formatOut = FULL_DATE_FORMAT) 
        binding.tvGenres.text = getGenres(movie.genres ?: emptyList())
        binding.tvVotes.text = "${movie.voteCount}"
        binding.tvStatusValue.text = movie.status ?: ""
        binding.tvBudgetValue.text = CurrencyFormat.formatUSD(movie.budget)
        binding.tvRuntimeValue.text = getString(R.string.detail_runtime_value, movie.runtime)
        binding.tvRevenueValue.text = CurrencyFormat.formatUSD(movie.revenue)
        binding.tvOverviewValue.text = movie.overview

        val url = "https://image.tmdb.org/t/p/w500${movie.backdropPath}" // TODO Quitar hardcode
        Glide.with(binding.root.context)
            .load(url)
            .into(binding.ivBackdrop)
    }

    private fun initDagger() {
        val movieListComponent = (requireActivity().applicationContext as MovieListComponentProvider)
            .provideMovieListComponent()
        movieListComponent.inject(this)
    }

    private fun getGenres(genres: List<Genre>): String {
        var genresStr = ""
        for(genre in genres) {
            if(genresStr.length > 0) {
                genresStr += ", "
            }
            genresStr += genre.name
        }

        return genresStr
    }
}