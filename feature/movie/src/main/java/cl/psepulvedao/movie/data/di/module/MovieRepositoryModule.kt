package cl.psepulvedao.movie.data.di.module

import cl.psepulvedao.movie.data.remote.RetrofitMovieRepository
import cl.psepulvedao.movie.data.remote.api.MovieApi
import cl.psepulvedao.movie.di.scope.MovieScope
import cl.psepulvedao.movie.domain.repository.MovieRepository
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class MovieRepositoryModule {

    @MovieScope
    @Provides
    fun provideMovieApi(retrofit: Retrofit): MovieApi = retrofit.create(MovieApi::class.java)

    @MovieScope
    @Provides
    fun provideMovieRepository(api: MovieApi): MovieRepository = RetrofitMovieRepository(api)

}