package cl.psepulvedao.movie.presentation.di.component

interface MovieListComponentProvider {
    fun provideMovieListComponent(): MovieListComponent
}