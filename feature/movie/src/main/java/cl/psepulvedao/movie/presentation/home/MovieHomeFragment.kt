package cl.psepulvedao.movie.presentation.home

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.View
import androidx.fragment.app.viewModels
import cl.psepulvedao.movie.R
import cl.psepulvedao.movie.databinding.MovieHomeFragmentBinding
import cl.psepulvedao.movie.presentation.di.component.MovieListComponentProvider
import cl.psepulvedao.movie.presentation.movie.MovieListViewModel
import javax.inject.Inject

class MovieHomeFragment : Fragment(R.layout.movie_home_fragment) {

    @Inject
    lateinit var providerFactory: ViewModelProvider.Factory

    private lateinit var movieListViewModel: MovieListViewModel
    private lateinit var binding: MovieHomeFragmentBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = MovieHomeFragmentBinding.bind(view)
        setupViews()

        movieListViewModel.logInstance()
        movieListViewModel.getPopular()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initDagger()
        movieListViewModel = ViewModelProvider(requireActivity(), providerFactory).get(MovieListViewModel::class.java)
    }

    private fun setupViews() {
        binding.bnOrderMovie.setOnNavigationItemSelectedListener { onNavigationItemSelected(it.itemId) }
    }

    private fun onNavigationItemSelected(option: Int): Boolean {
        when(option) {
            R.id.popular -> movieListViewModel.getPopular()
            R.id.top_rated -> movieListViewModel.getTopRated()
            R.id.upcoming -> movieListViewModel.getUpcoming()
            R.id.now_playing -> movieListViewModel.getNowPlaying()
        }
        return true
    }

    private fun initDagger() {
        val movieListComponent = (requireActivity().applicationContext as MovieListComponentProvider)
            .provideMovieListComponent()
        movieListComponent.inject(this)
    }

}