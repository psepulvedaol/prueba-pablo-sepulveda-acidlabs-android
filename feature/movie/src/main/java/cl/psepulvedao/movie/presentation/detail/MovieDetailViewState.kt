package cl.psepulvedao.movie.presentation.detail

import cl.psepulvedao.core.ui.base.BaseViewState
import cl.psepulvedao.movie.domain.model.Movie

sealed class MovieDetailViewState: BaseViewState {
    object Loading: MovieDetailViewState()
    class LoadedDetail(val movie: Movie): MovieDetailViewState()
}