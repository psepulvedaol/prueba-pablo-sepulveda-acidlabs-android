package cl.psepulvedao.movie.presentation.di.module

import androidx.lifecycle.ViewModelProvider
import cl.psepulvedao.movie.di.scope.MovieScope
import cl.psepulvedao.movie.presentation.di.viewmodel.ViewModelProviderFactory
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ViewModelFactoryModule {

//    @Binds
//    abstract fun bindViewModelFactory(viewModelProviderFactory: ViewModelProviderFactory)
//        : ViewModelProvider.Factory

    @MovieScope
    @Provides
    fun  bindViewModelFactory(factory: ViewModelProviderFactory): ViewModelProvider.Factory {
        return factory;
    }

}