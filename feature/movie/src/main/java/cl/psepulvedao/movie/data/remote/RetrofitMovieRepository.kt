package cl.psepulvedao.movie.data.remote

import android.util.Log
import cl.psepulvedao.movie.data.remote.api.MovieApi
import cl.psepulvedao.movie.data.remote.response.GetMovieListResponseMapper
import cl.psepulvedao.movie.data.remote.scheme.MovieMapper
import cl.psepulvedao.movie.domain.model.Movie
import cl.psepulvedao.movie.domain.model.MovieList
import cl.psepulvedao.movie.domain.repository.MovieRepository
import io.reactivex.Observable
import javax.inject.Inject

class RetrofitMovieRepository @Inject constructor(val api: MovieApi): MovieRepository {
    override fun getMovieList(page: Int, type: String): Observable<MovieList> = Observable.create {
        try {
            val request = mapOf(
                "page" to page.toString(),
                "api_key" to "e9f998c60ecb4acfeaeab40b1e24de89" // Eliminar hardcode
            )
            val response = api.getMovieList(type, request).execute()
            val body = response.body()

            if(response.isSuccessful && body != null) {
                it.onNext(GetMovieListResponseMapper.mapToModelMovieList(body))
            }
        } catch(ex: Exception) {
            Log.e("MovieRepository", "Error al obtener las películas populares", ex)
        }
    }

    override fun getMovieById(movieId: Int): Observable<Movie> = Observable.create {
        try {
            val request = mapOf(
                "api_key" to "e9f998c60ecb4acfeaeab40b1e24de89" // Eliminar hardcode
            )
            val response = api.getMovieById(movieId, request).execute()
            val body = response.body()

            if(response.isSuccessful && body != null) {
                it.onNext(MovieMapper.mapToModelMovie(body))
            }
        } catch(ex: Exception) {
            Log.e("MovieRepository", "Error al obtener las películas populares", ex)
        }
    }
}