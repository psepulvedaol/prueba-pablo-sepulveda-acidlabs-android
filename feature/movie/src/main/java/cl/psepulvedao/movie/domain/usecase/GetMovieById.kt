package cl.psepulvedao.movie.domain.usecase

import cl.psepulvedao.movie.domain.repository.MovieRepository
import javax.inject.Inject

class GetMovieById @Inject constructor(private val repository: MovieRepository) {
    operator fun invoke(movieId: Int) = repository.getMovieById(movieId)
}