package cl.psepulvedao.movie.data.remote.response

import cl.psepulvedao.movie.data.remote.scheme.Movie
import com.google.gson.annotations.SerializedName

data class GetMovieListResponse (
    @SerializedName("page") val page: Int,
    @SerializedName("total_results") val totalResults: Int,
    @SerializedName("total_pages") val totalPages: Int,
    @SerializedName("results") val results: List<Movie>
)