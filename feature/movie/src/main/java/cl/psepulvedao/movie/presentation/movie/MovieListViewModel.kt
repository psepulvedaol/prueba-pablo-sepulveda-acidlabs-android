package cl.psepulvedao.movie.presentation.movie

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cl.psepulvedao.movie.domain.model.MovieType
import cl.psepulvedao.movie.domain.usecase.GetMovieListUseCase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MovieListViewModel @Inject constructor(
    private val getMovieListUseCase: GetMovieListUseCase
): ViewModel() {

    private val _state = MutableLiveData<MovieListViewState>()
    val state: LiveData<MovieListViewState> = _state

    var lastPageLoaded = 0
    var typeLoaded = MovieType.POPULAR
    var isLastPage = false

    fun logInstance() {
        Log.d("ViewState", this.toString())
    }

    private fun reset(type: String) {
        lastPageLoaded = 0
        typeLoaded = type
    }

    fun getPopular() {
        reset(MovieType.POPULAR)
        getMovies()
    }

    fun getTopRated() {
        reset(MovieType.TOP_RATED)
        getMovies()
    }

    fun getUpcoming() {
        reset(MovieType.UPCOMING)
        getMovies()
    }

    fun getNowPlaying() {
        reset(MovieType.NOW_PLAYING)
        getMovies()
    }

    fun getMovies() {
        val page = lastPageLoaded + 1
        _state.value = MovieListViewState.Loading

        getMovieListUseCase(page, typeLoaded)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext { movieList ->
                lastPageLoaded = page
                isLastPage = lastPageLoaded == movieList.totalPages
                _state.value = MovieListViewState.LoadedPage(lastPageLoaded, movieList)
            }.subscribe() // TODO manejar error
    }

}