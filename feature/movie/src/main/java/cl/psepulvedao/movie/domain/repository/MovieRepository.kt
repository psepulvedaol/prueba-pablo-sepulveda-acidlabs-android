package cl.psepulvedao.movie.domain.repository

import cl.psepulvedao.movie.domain.model.Movie
import cl.psepulvedao.movie.domain.model.MovieList
import io.reactivex.Observable

interface MovieRepository {

    fun getMovieList(page: Int, type: String): Observable<MovieList>
    fun getMovieById(movieId: Int): Observable<Movie>

}