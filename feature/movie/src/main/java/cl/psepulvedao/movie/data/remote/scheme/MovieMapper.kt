package cl.psepulvedao.movie.data.remote.scheme

object MovieMapper {
    fun mapToModelMovie(movie: Movie) = cl.psepulvedao.movie.domain.model.Movie(
        id = movie.id,
        title = movie.title,
        posterPath = movie.posterPath,
        backdropPath = movie.backdropPath,
        budget = movie.budget,
        genres = GenreMapper.mapListToModelGenre(movie.genres ?: emptyList()),
        homepage = movie.homepage,
        overview = movie.overview,
        releaseDate = movie.releaseDate,
        revenue = movie.revenue,
        runtime = movie.runtime,
        status = movie.status,
        voteCount = movie.voteCount,
        voteAverage = movie.voteAverage
    )
}