package cl.psepulvedao.movie.presentation.di.module

import androidx.lifecycle.ViewModel
import cl.psepulvedao.movie.di.scope.MovieScope
import cl.psepulvedao.movie.presentation.detail.MovieDetailViewModel
import cl.psepulvedao.movie.presentation.di.viewmodel.ViewModelKey
import cl.psepulvedao.movie.presentation.movie.MovieListViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class MovieViewModelModule {

    @MovieScope
    @Binds
    @IntoMap
    @ViewModelKey(MovieListViewModel::class)
    abstract fun bindsMovieListViewModel(viewModel: MovieListViewModel): ViewModel

    @MovieScope
    @Binds
    @IntoMap
    @ViewModelKey(MovieDetailViewModel::class)
    abstract fun bindsMovieDetailViewModel(viewModel: MovieDetailViewModel): ViewModel

}