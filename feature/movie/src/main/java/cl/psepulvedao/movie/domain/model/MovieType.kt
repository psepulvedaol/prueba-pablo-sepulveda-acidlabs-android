package cl.psepulvedao.movie.domain.model

object MovieType {
    const val POPULAR = "popular"
    const val NOW_PLAYING = "now_playing"
    const val TOP_RATED = "top_rated"
    const val UPCOMING = "upcoming"
}