package cl.psepulvedao.movie.domain.usecase

import cl.psepulvedao.movie.domain.repository.MovieRepository
import javax.inject.Inject

class GetMovieListUseCase @Inject constructor(private val repository: MovieRepository) {
    operator fun invoke(page: Int, type: String) = repository.getMovieList(page, type)
}