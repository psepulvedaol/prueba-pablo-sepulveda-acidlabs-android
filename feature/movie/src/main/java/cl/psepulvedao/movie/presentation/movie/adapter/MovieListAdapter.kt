package cl.psepulvedao.movie.presentation.movie.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import cl.psepulvedao.movie.databinding.ItemMovieBinding
import cl.psepulvedao.movie.domain.model.Movie

class MovieListAdapter(private val movies: MutableList<Movie>, private val listener:(movie: Movie) -> Unit):
    RecyclerView.Adapter<MovieViewHolder>() {

    fun addData(movies: List<Movie>) {
        this.movies.addAll(movies)
        notifyDataSetChanged() // TODO notificar por rango
    }

    fun setData(movies: List<Movie>) {
        this.movies.clear()
        this.movies.addAll(movies)

        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val binding = ItemMovieBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MovieViewHolder(binding, listener)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.bind(movies[position])
    }

    override fun getItemCount(): Int = movies.size
}