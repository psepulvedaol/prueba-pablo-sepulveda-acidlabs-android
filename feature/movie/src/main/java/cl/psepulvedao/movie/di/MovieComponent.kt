package cl.psepulvedao.movie.di

import cl.psepulvedao.core.di.CoreComponent
import cl.psepulvedao.movie.di.module.MovieModule
import cl.psepulvedao.movie.di.scope.MovieScope
import dagger.Component

@MovieScope
@Component(
    modules = [MovieModule::class],
    dependencies = [CoreComponent::class]
)
interface MovieComponent {
}