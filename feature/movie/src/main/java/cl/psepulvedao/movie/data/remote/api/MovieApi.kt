package cl.psepulvedao.movie.data.remote.api

import cl.psepulvedao.movie.data.remote.response.GetMovieListResponse
import cl.psepulvedao.movie.data.remote.scheme.Movie
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.QueryMap

interface MovieApi {
    @Headers("Accept: application/json")
    @GET("movie/{type}")
    fun getMovieList(@Path("type") type: String, @QueryMap request: Map<String, String>): Call<GetMovieListResponse>

    @Headers("Accept: application/json")
    @GET("movie/{movieId}")
    fun getMovieById(@Path("movieId") movieId: Int, @QueryMap request: Map<String, String>): Call<Movie>

}