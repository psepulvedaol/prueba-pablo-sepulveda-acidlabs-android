package cl.psepulvedao.movie.presentation.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cl.psepulvedao.movie.domain.usecase.GetMovieById
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MovieDetailViewModel @Inject constructor(private val getMovieById: GetMovieById) : ViewModel() {

    private val _state = MutableLiveData<MovieDetailViewState>()
    val state: LiveData<MovieDetailViewState> = _state

    fun getMovieDetail(movieId: Int) {
        _state.value = MovieDetailViewState.Loading
        getMovieById(movieId)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext { movie ->
                _state.value = MovieDetailViewState.LoadedDetail(movie)
            }.subscribe() // TODO manejar error
    }

}