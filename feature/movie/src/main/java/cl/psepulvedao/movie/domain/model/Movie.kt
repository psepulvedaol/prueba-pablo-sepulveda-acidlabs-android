package cl.psepulvedao.movie.domain.model

data class Movie(
    val id: Int,
    val title: String,
    val posterPath: String? = null,
    val backdropPath: String? = null,
    val budget: Long,
    val genres: List<Genre>? = null,
    val homepage: String? = null,
    val overview: String? = null,
    val releaseDate: String? = null,
    val revenue: Long,
    val runtime: Int,
    val status: String? = null,
    val voteCount: Int,
    val voteAverage: Float
)