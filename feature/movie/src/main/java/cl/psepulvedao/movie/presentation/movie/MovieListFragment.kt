package cl.psepulvedao.movie.presentation.movie

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.View
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import cl.psepulvedao.core.ui.listener.PaginationScrollListener
import cl.psepulvedao.movie.R
import cl.psepulvedao.movie.databinding.MovieListFragmentBinding
import cl.psepulvedao.movie.domain.model.Movie
import cl.psepulvedao.movie.domain.model.MovieList
import cl.psepulvedao.movie.presentation.di.component.MovieListComponentProvider
import cl.psepulvedao.movie.presentation.home.MovieHomeFragmentDirections
import cl.psepulvedao.movie.presentation.movie.adapter.MovieListAdapter
import cl.psepulvedao.ui.extension.gone
import cl.psepulvedao.ui.extension.visible
import javax.inject.Inject

class MovieListFragment : Fragment(R.layout.movie_list_fragment) {

    @Inject
    lateinit var providerFactory: ViewModelProvider.Factory

    private lateinit var moviesAdapter: MovieListAdapter

//    private val movieListViewModel: MovieListViewModel by lazy {
//        ViewModelProvider(this, providerFactory).get(MovieListViewModel::class.java)
//    }
    private lateinit var movieListViewModel: MovieListViewModel

    private lateinit var binding: MovieListFragmentBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initDagger()
        movieListViewModel = ViewModelProvider(requireActivity(), providerFactory).get(MovieListViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = MovieListFragmentBinding.bind(view)

        setupViews()
        setupObservers()
        movieListViewModel.logInstance()
    }

    private fun initDagger() {
        (requireActivity().applicationContext as MovieListComponentProvider)
            .provideMovieListComponent()
            .inject(this)
    }

    private fun setupViews() {
        moviesAdapter = MovieListAdapter(mutableListOf(), ::goToDetail)
        val layoutManager = GridLayoutManager(requireContext(), 2)
        binding.rvMovies.adapter = moviesAdapter
        binding.rvMovies.layoutManager = layoutManager

        binding.rvMovies.addOnScrollListener(object: PaginationScrollListener(layoutManager, 10) {
            override fun loadMoreItems() = movieListViewModel.getMovies()

            override fun isLastPage(): Boolean = movieListViewModel.isLastPage

            override fun isLoading(): Boolean =
                movieListViewModel.state.value == MovieListViewState.Loading
        })
    }

    private fun setupObservers() {
        Log.d("ViewState", "setupObservers")
        movieListViewModel.state.observe(viewLifecycleOwner, { handleViewState(it) })
    }

    private fun handleViewState(viewState: MovieListViewState) {
        Log.d("ViewState", "handleViewState")
        Log.d("ViewState", "handleViewState: ${viewState::class.java.simpleName}")
        when(viewState) {
            MovieListViewState.Loading -> loadingFirstPage()
            is MovieListViewState.LoadedPage -> loadedPage(viewState.page, viewState.movieList)
        }
    }

    private fun loadingFirstPage() {
//        binding.rvMovies.gone()
//        binding.progressBar.visible()
    }

    private fun loadedPage(page: Int, movieList: MovieList) {
        binding.rvMovies.visible()
        binding.progressBar.gone()

        if(page == 1) {
            moviesAdapter.setData(movieList.results)
        } else {
            moviesAdapter.addData(movieList.results)
        }
    }

    private fun goToDetail(movie: Movie) {
        val direction = MovieHomeFragmentDirections
            .actionMovieFragmentToMovieDetailFragment(movie.id)
        findNavController().navigate(direction)
    }
}