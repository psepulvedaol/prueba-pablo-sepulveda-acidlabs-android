package cl.psepulvedao.movie.data.remote.scheme

object GenreMapper {
    fun mapToModelGenre(genre: Genre) = cl.psepulvedao.movie.domain.model.Genre(
        id = genre.id,
        name = genre.name
    )

    fun mapListToModelGenre(genres: List<Genre>): List<cl.psepulvedao.movie.domain.model.Genre> {
        val modelGenres = mutableListOf<cl.psepulvedao.movie.domain.model.Genre>()
        for(g in genres) {
            modelGenres.add(mapToModelGenre(g))
        }

        return modelGenres
    }
}