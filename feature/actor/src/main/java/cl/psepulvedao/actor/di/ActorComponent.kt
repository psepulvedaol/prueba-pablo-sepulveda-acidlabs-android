package cl.psepulvedao.actor.di

import cl.psepulvedao.actor.di.module.ActorModule
import cl.psepulvedao.actor.di.scope.ActorScope
import cl.psepulvedao.core.di.CoreComponent
import dagger.Component

@ActorScope
@Component(
    dependencies = [CoreComponent::class],
    modules = [ActorModule::class]
)
interface ActorComponent {
}