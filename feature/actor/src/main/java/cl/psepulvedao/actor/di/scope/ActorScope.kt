package cl.psepulvedao.actor.di.scope

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.SOURCE)
annotation class ActorScope